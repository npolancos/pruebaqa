package co.com.choucair.certification.NicolasPolanco.stepdefinitions;
import co.com.choucair.certification.NicolasPolanco.questions.Answer;
import co.com.choucair.certification.NicolasPolanco.tasks.Login;
import co.com.choucair.certification.NicolasPolanco.tasks.OpenUp;
import co.com.choucair.certification.NicolasPolanco.tasks.Search;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

public class ChoucairAcademyStepDefintions {
    @Before
    public void setStage(){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^than Nicolas wants to learn automation at the academy Choucair$")
    public void thanNicolasWantsToLearnAutomationAtTheAcademyChoucair()  {
        OnStage.theActorCalled("Nicolas").wasAbleTo(OpenUp.thePage(), (Login.onThePage()));


    }


    @When("^he search for the course (.*) on the choucair academy platform$")
    public void heSearchForTheCourseRecursosAutomatizaciónBancolombiaOnTheChoucairAcademyPlatform(String course)  {
        OnStage.theActorInTheSpotlight().attemptsTo(Search.the(course));

    }

    @Then("^he finds the course called resources (.*)$")
    public void heFindsTheCourseCalledResourcesRecursosAutomatizaciónBancolombia(String question)  {

        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Answer.toThe(question)));


    }

}
