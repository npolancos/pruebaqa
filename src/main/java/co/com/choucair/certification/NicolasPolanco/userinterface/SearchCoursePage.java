package co.com.choucair.certification.NicolasPolanco.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class SearchCoursePage extends PageObject {

    public static final Target BUTTON_UC = Target.the("seleccionar la universidad choucair").located(By.xpath("//div[@id='universidad']//strong"));
    public static final Target INPUT_COURSE = Target.the("Buscar el curso").located(By.id("coursesearchbox"));
    public static final Target BUTTON_GO = Target.the("Dar click para buscar el curso").located(By.xpath("//BUTTON[@class='btn btn-secondary'][text()='Ir']"));
    public static final Target SELECT_COURSE = Target.the("Dar click para buscar el curso").located(By.xpath("//H4[text()='Recursos Automatización Bancolombia']"));
    public static final Target NAME_COURSE = Target.the("Extrae el nombre del curso").located(By.xpath("//H1[text()='Recursos Automatización Bancolombia']"));





}
